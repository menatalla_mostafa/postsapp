import { NgModule } from '@angular/core';
import { FooterComponent } from './components/footer/footer.component';
import { HeaderComponent } from './components/header/header.component';
import { MaterialModule } from './modules/material/material.module'
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
    imports: [MaterialModule, FlexLayoutModule, ReactiveFormsModule, FormsModule],
    declarations: [FooterComponent, HeaderComponent],
    providers: [],
    exports: [FooterComponent, HeaderComponent, MaterialModule, FlexLayoutModule, ReactiveFormsModule, FormsModule]
})

export class SharedModule { }