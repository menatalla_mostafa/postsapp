import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ListComponent } from './list/list.component';
import { AddEditPostComponent } from './add-edit-post/add-edit-post.component'

const routes: Routes = [
  {
    path: '',
    redirectTo: 'list',
    pathMatch: 'full'
  },
  {
    component: ListComponent,
    path: 'list'
  },
  {
    component: AddEditPostComponent,
    path: 'add',
  },
  {
    component: AddEditPostComponent,
    path: 'edit/:id',
  },
  {
    path: '**',
    redirectTo: 'list',
    pathMatch: 'full'
  }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PostsRoutingModule { }
