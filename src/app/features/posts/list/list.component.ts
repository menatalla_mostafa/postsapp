import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatTable } from '@angular/material/table';
import { PostsService } from './../../../core/services/posts/posts.service';
import { IPost } from './../../../shared/models/posts.model';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { AddEditPostComponent } from './../add-edit-post/add-edit-post.component'
import { Location } from '@angular/common';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {
  @ViewChild(MatTable, { static: true }) table: MatTable<any>;

  postsList: IPost[] = [];
  displayedColumns: string[] = ['title', 'body', 'actions'];
  @ViewChild(MatPaginator, { static: true })
  paginator: MatPaginator;
  pageSize: number = 10;
  postsLength: number = 100;

  constructor(private location: Location, private postsService: PostsService, public dialog: MatDialog) { }

  ngOnInit(): void {
    this.postsService.get().subscribe(response => {
      this.postsList = response;
    });

    this.paginator._intl.itemsPerPageLabel = 'Page';
    this.paginator._intl.getRangeLabel = (page: number, pageSize: number, length: number) => {
      const start = page * pageSize + 1;
      const end = (page + 1) * pageSize;
      return `${this.paginator._intl.itemsPerPageLabel} ${start} - ${end}`;
    };
  }

  changePage(page: any) {
  }

  delete(post: IPost): void {
    let id = post.id;
    this.postsService.delete(id).subscribe(response => {
      this.postsList = this.postsList.filter(post => {
        return post.id != id
      });
    })
  }

  openPostDialog(post?: IPost): void {
    let path = post ? `/edit/${post.id}` : '/add';
    this.location.replaceState(path);

    const dialogRef = this.dialog.open(AddEditPostComponent, {
      width: '75%',
      data: post,
      disableClose: true
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        let req = post ? this.postsService.post(result.data) : this.postsService.post(result.data);
        req.subscribe((response: IPost) => {
          if (path == '/add') {
            this.postsList.unshift(response);
            this.table.renderRows();
            return
          }
          let indx = this.postsList.findIndex(post => post.id == post.id);
          this.postsList[indx] = response;
          this.table.renderRows();
        })
      }
      this.location.replaceState('/list');
    });
  }
}
