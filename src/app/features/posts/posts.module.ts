import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PostsRoutingModule } from './posts-routing.module';
import { ListComponent } from './list/list.component';
import { AddEditPostComponent } from './add-edit-post/add-edit-post.component';
import { SharedModule } from './../../shared/shared.module'
@NgModule({
  declarations: [
    ListComponent,
    AddEditPostComponent
  ],
  imports: [
    CommonModule,
    PostsRoutingModule,
    SharedModule
  ]
})
export class PostsModule { }
