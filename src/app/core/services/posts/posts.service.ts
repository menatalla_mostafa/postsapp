import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { environment } from './../../../../environments/environment'
import { IPost } from './../../../shared/models/posts.model';
import { retry, catchError } from 'rxjs/operators';
import { Observable, throwError } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PostsService {
  route: string = 'posts';
  constructor(private httpClient: HttpClient) { }

  get(): Observable<IPost[]> {
    return this.httpClient.get<IPost[]>(this.createCompleteRoute(), {}).pipe(
      retry(1),
      catchError(this.processError)
    );
  }

  post(post: IPost) {
    return this.httpClient.post(this.createCompleteRoute(), post, this.generateHeaders()).pipe(
      retry(1),
      catchError(this.processError)
    );
  }

  put(post: IPost) {
    return this.httpClient.put(this.createCompleteRoute(), post, this.generateHeaders()).pipe(
      retry(1),
      catchError(this.processError)
    );
  }

  delete(id: number) {
    let httpParams = new HttpParams();
    httpParams = httpParams.append('id', id);
    return this.httpClient.delete(`${this.createCompleteRoute()}/${id}`).pipe(
      retry(1),
      catchError(this.processError)
    );
  }
  private createCompleteRoute = () => {
    return `${environment.urlAddress}/${this.route}`;
  }

  private generateHeaders = () => {
    return {
      headers: new HttpHeaders({ 'Content-Type': 'application/json' })
    }
  }

  processError(err) {
    let message = '';
    if (err.error instanceof ErrorEvent) {
      message = err.error.message;
    } else {
      message = `Error Code: ${err.status}\nMessage: ${err.message}`;
    }
    console.log(message);
    return throwError(message);
  }
}
