import { NgModule, Optional, SkipSelf } from '@angular/core';
import { PostsService } from './services/posts/posts.service'

@NgModule({
  declarations: [],
  imports: [],
  providers: [PostsService]
})
export class CoreModule {
  constructor(@Optional() @SkipSelf() coreModule: CoreModule) {
    if (coreModule) {
      throw new Error("GGG");
    }
  }
}
